import { Component, OnInit } from '@angular/core';
import { Firebase } from "../../providers/firebase";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
	private user;
	private password;

	constructor(private firebase: Firebase, private router: Router) {
			this.firebase.auth().onAuthStateChanged(user => {
				if (user) {
					this.router.navigate(['/home']);
				}
			});

	}


	onClickEntrar() {
		this.firebase.auth().signInWithEmailAndPassword(this.user, this.password).then(user => {
			this.router.navigate(['/home']);
		}, error => {
			console.log(error);
		});


	}

}
