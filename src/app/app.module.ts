import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Firebase } from '../providers/firebase';

import { AppComponent } from './app.component';
import { LoginComponent } from '../pages/login/login.component';
import { HomeComponent } from '../pages/home/home.component';

import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from '../providers/auth-guard';

@NgModule({
  declarations: [
    AppComponent,
		LoginComponent,
		HomeComponent
  ],
  imports: [
		AppRoutingModule,
    BrowserModule,
		FormsModule,
		AppRoutingModule,
  ],
  providers: [Firebase, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
