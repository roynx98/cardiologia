import { Injectable } from '@angular/core';
import * as firebase from "firebase";

@Injectable()
export class Firebase {

	constructor() {
		let config = {
			apiKey: "AIzaSyB68uY7mGZ5fX7abqB_Mm8C6PlBSb4nMpk",
			authDomain: "control-efa59.firebaseapp.com",
			databaseURL: "https://control-efa59.firebaseio.com",
			projectId: "control-efa59",
			storageBucket: "control-efa59.appspot.com",
			messagingSenderId: "745749748817"		
		};

		firebase.initializeApp(config);
	}

	public auth() {
		return firebase.auth();
	}

	public database() {
		return firebase.database();
	}

	public storage() {
		return firebase.storage();
	}

}
