import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Firebase } from './firebase';

@Injectable()
export class AuthGuard implements CanActivate {

	private isLogin = null;

	constructor(private router: Router, private firebase: Firebase) {

	}

	canActivate() {
		if (this.isLogin != null)
			return this.isLogin;

		return new Promise(resolve => {
			this.firebase.auth().onAuthStateChanged(user => {

				if (user) {
					this.isLogin = true;

					let userID = this.firebase.auth().currentUser.uid;

					resolve(this.isLogin);
					/*
					this.firebase.database().ref('users/' + userID).once('value').then(snap => {
						resolve(this.isLogin);
					});
					 */

				} else {
					resolve(this.isLogin);
					this.router.navigate(['/login']);
				}

			});


		});
	}

}
